import React, { Component } from 'react';
import Home from './Home';
import { ListGroup } from 'react-bootstrap';

export default class Result extends Component {

    render() {
        let textAlign = {
          textAlign: "left",
        };
        return (
          <div>
            <ListGroup.Item style={textAlign}>{this.props.output}</ListGroup.Item>
          </div>
        );
    }
}
