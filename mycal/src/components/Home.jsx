import React, { Component } from 'react';

import {Card,Button,Form,ListGroup} from 'react-bootstrap';
import Result from './Result';

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
          numbers : []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleSubmit(event) {
        event.preventDefault();
        var num1 = Number(this.n1.value);
        let num2 = Number(this.n2.value);
        let operator = this.option.value;
        var result;
    
        switch (operator) {
          case "+":
            result = num1 + num2;
            break;
          case "-":
            result = num1 - num2;
            break;
          case "x":
            result = num1 * num2;
            break;
          case "/":
            result = num1 / num2;
            break;
          case "%":
            result = num1 % num2;
            break;
          default:
            alert("Please choose operator first!!!");
            break;
        }
    
        this.setState({
            numbers: this.state.numbers.concat(result),
        });
      }
    
      render() {
    
        var item = this.state.numbers.map((ele, index) => (
            <Result output={ele} key={index} />
          ));
    
        return (
          <div>
            <form onSubmit={this.handleSubmit}>
              <div className="container App">
                <div className="row pt-4">
                  <div className="col-md-4">
                    <Card>
                      <Card.Img variant="top" src={require('./image/zi.png')} />
                      <Card.Body>
                        <Form.Group controlId="formBasicEmail">
                          <Form.Control
                            type="number"
                            placeholder="Enter number"
                            ref={(num1) => (this.n1 = num1)}
                            name="n1"
                          />
                        </Form.Group>
    
                        <Form.Group controlId="formBasicEmail">
                          <Form.Control
                            type="number"
                            placeholder="Enter number"
                            ref={(num2) => (this.n2 = num2)}
                            name="n2"
                          />
                        </Form.Group>
    
                        <Form.Group controlId="">
                          <Form.Control as="select" custom ref={(choose) => (this.option = choose)} name="option">
                            <option value="+">+ &nbsp;  Plus</option>
                            <option value="-">- &nbsp;  Sub</option>
                            <option value="x">x &nbsp;  Multi</option>
                            <option value="/">/ &nbsp;  Division</option>
                            <option value="%">%  Module</option>
                          </Form.Control>
                        </Form.Group>
                        <Button variant="primary" type="submit">
                          Submit
                        </Button>
                      </Card.Body>
                    </Card>
                  </div>
    
                  <div className="col-md-4">
                  <h1>Result history: </h1> <br/>
                  <ListGroup>{item}</ListGroup>
                  </div>
                </div>
              </div>
            </form>
          </div>
  
        ); 
    }
}
